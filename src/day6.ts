import test from "ava";

import * as R from "remeda";
import { readInput } from "./util/input";

const sampleInput = (): string => {
  return `mjqjpqmgbljsphdztnvjfqwrcgsmlb`;
};

const part1 = (input: string): number => {
  let buf: string[] = [];
  let count = 0;
  R.takeWhile(input.split(""), (x: string): boolean => {
    count++;
    buf.push(x);
    if (buf.length > 4) {
      buf = buf.slice(1);
    }
    if (buf.length === 4) {
      if (R.equals(R.uniq(buf), buf)) {
        console.log({ uniq: R.uniq(buf), buf });
        return false;
      }
    }
    return true;
  });
  return count;
};

const part2 = (input: string): number => {
  let buf: string[] = [];
  let count = 0;
  R.takeWhile(input.split(""), (x: string): boolean => {
    count++;
    buf.push(x);
    if (buf.length > 14) {
      buf = buf.slice(1);
    }
    if (buf.length === 14) {
      if (R.equals(R.uniq(buf), buf)) {
        console.log({ uniq: R.uniq(buf), buf });
        return false;
      }
    }
    return true;
  });
  return count;
};

test("part 1 sample input", (t) => {
  t.is(7, part1(sampleInput()));
});

test("part 1 full input", (t) => {
  const part1res = part1(readInput(6));
  console.log(`Day 6, Part 1: ${part1res}`);
  t.pass();
});

test("part 2 sample input", (t) => {
  t.is(19, part2(sampleInput()));
});

test("part 2 full input", (t) => {
  const part2res = part2(readInput(6));
  console.log(`Day 6, Part 2: ${part2res}`);
  t.pass();
});

import test from "ava";
import * as R from 'remeda';

import { readInputLines } from './util/input';

const shortInput = (): string[] => {
  const data = `vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
`;
  return data.split("\n");
};

const rucksackDuplicate = (sack: string): string | undefined => {
  const leftHalf = sack.substring(0, sack.length / 2);
  const rightHalf = sack.substring(sack.length / 2);

  const _set = new Set();
  leftHalf.split("").forEach((chr) => {
    _set.add(chr);
  });
  const res = rightHalf.split("").find((chr) => {
    return _set.has(chr);
  });
  return res;
};

const alphaScores: string[] =
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

const part1 = (input: string[]): number => {
  const total = input.reduce((memo, sack) => {
    const dupe = rucksackDuplicate(sack);
    if (dupe) {
      return memo + (alphaScores.indexOf(dupe) + 1);
    }
    return memo;
  }, 0);

  return total;
};

const part2 = (input: string[]): number => {
  const trios = R.chunk(input, 3);
  let total = trios.reduce((memo, trio) => {
    const map = new Map<string, number>();
    trio.forEach((sack) => {
      const set = new Set(sack.split(""));
      set.forEach((chr) => {
        if (map.has(chr)) {
          map.set(chr, map.get(chr)! + 1);
        } else {
          map.set(chr, 1);
        }
      });
    });
    for (const [chr, count] of map.entries()) {
      if (count === 3) {
        return memo + (alphaScores.indexOf(chr) + 1);
      }
    }
    return memo;
  }, 0);

  return total;
};

test("part1 test data", (t) => {
  t.is(157, part1(shortInput()));
});

test("part1 full data", (t) => {
  const part1res = part1(readInputLines(3));
  console.log(`Day 3, Part 1: ${part1res}`);
  t.pass();
});

test("rucksackDuplicate", (t) => {
  const str = "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL";
  t.is("L", rucksackDuplicate(str) ?? "");
});

test("part2 test data", (t) => {
  t.is(70, part2(shortInput()));
});

test("part2 full data", (t) => {
  const part2res = part2(readInputLines(3));
  console.log(`Day 3, Part 2: ${part2res}`);
  t.pass();
});

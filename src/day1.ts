import * as R from "remeda";
import test from "ava";

import { readInput } from "./util/input";

type Elf = number[];

const buildElves = (data: string): Elf[] => {
  let elves: Elf[] = [];
  let currentElf: number[] = [];
  for (const line of data.split("\n")) {
    if (line === "") {
      elves.push(currentElf);
      currentElf = [];
    } else {
      currentElf.push(Number.parseInt(line));
    }
  }
  return elves;
};

const sum = (elf: Elf): number => {
  return elf.reduce((memo, n) => memo + n);
};

const part1 = (input: string): number => {
  const elves = buildElves(input);
  const orderedElves = R.sortBy(elves, (elf) => sum(elf));
  return sum(orderedElves[orderedElves.length - 1]);
};

const part2 = (input: string): number => {
  const elves = buildElves(input);
  const orderedElves = R.sortBy(elves, (elf) => sum(elf));

  const lastThree = R.take(R.reverse(orderedElves), 3);
  return lastThree
    .map((elf) => sum(elf))
    .reduce((sum, elfCalories) => sum + elfCalories);
};

test("part1 full", (t) => {
  const part1res = part1(readInput(1));
  console.log(`Day 1, Part 1: ${part1res}`);
  t.pass();
});

test("part2 full", (t) => {
  const part2res = part2(readInput(1));
  console.log(`Day 1, Part 2: ${part2res}`);
  t.pass();
});

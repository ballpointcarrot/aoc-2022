import test from "ava";

import * as R from "remeda";
import { readInputLines } from "./util/input";

type Crate = string;

type CrateColumn = Crate[];

type CrateStore = CrateColumn[];

interface Move {
  count: number;
  moveFrom: number;
  moveTo: number;
}

const shortInput = (): string[] => {
  return readInputLines(5, true);
};

const readCrateStore = (input: string[]): CrateStore => {
  let foundLine = false;
  const crateLines = R.reverse(input).filter((line) => {
    if (foundLine) {
      return true;
    }
    if (line.match(/(\d\s)+$/)) {
      foundLine = true;
    }
    return false;
  });

  const cratesByLine = crateLines.map((line: string) => {
    return R.chunk(line.split(""), 4).map((a) => {
      return a.filter((ch) => ch.match(/\w/))[0];
    });
  });
  let crateColumns: CrateStore;
  crateColumns = new Array(cratesByLine[0].length);
  cratesByLine.forEach((line) => {
    R.forEach.indexed(line, (crate, idx) => {
      if (crateColumns[idx] === undefined) {
        crateColumns[idx] = [];
      }
      if (crate !== undefined) {
        crateColumns[idx].push(crate);
      }
    });
  });
  return crateColumns;
};

const readMoves = (input: string[]): Move[] => {
  const moves = input.map((line) => {
    if (!line.match(/move/)) {
      return undefined;
    }
    const digits = line.match(/move (\d+) from (\d+) to (\d+)/);
    if (digits) {
      const [_, ct, frm, to] = digits;
      return {
        count: parseInt(ct),
        moveFrom: parseInt(frm),
        moveTo: parseInt(to),
      };
    }
  });
  return moves.filter((m) => m !== undefined) as Move[];
};
const part1 = (input: string[]): string => {
  const crateStore = readCrateStore(input);
  const moves = readMoves(input);
  moves.forEach((move) => {
    const from = crateStore[move.moveFrom - 1];
    const to = crateStore[move.moveTo - 1];
    for (let i = 0; i < move.count; i++) {
      let crate = from.pop();
      if (!crate) {
        throw new Error("Empty column!");
      }
      to.push(crate);
    }
  });
  return crateStore.map((col) => R.last(col)).join("");
};

const part2 = (input: string[]): string => {
  const crateStore = readCrateStore(input);
  const moves = readMoves(input);
  moves.forEach((move) => {
    const from = crateStore[move.moveFrom - 1];
    const to = crateStore[move.moveTo - 1];
    let crateStack: CrateColumn = [];
    for (let i = 0; i < move.count; i++) {
      let crate = from.pop();
      if (!crate) {
        throw new Error("Empty column!");
      }
      crateStack.push(crate);
    }
    while (crateStack.length > 0) {
      to.push(crateStack.pop()!);
    }
  });
  return crateStore.map((col) => R.last(col)).join("");
};

test("part1 test data", (t) => {
  t.is("CMZ", part1(shortInput()));
});

test("part1 full data", (t) => {
  const part1res = part1(readInputLines(5));
  console.log(`Day 5, Part 1: ${part1res}`);
  t.pass();
});

test("part2 test data", (t) => {
  t.is("MCD", part2(shortInput()));
});

test("part2 full data", (t) => {
  const part2res = part2(readInputLines(5));
  console.log(`Day 5, Part 2: ${part2res}`);
  t.pass();
});

test("readCrateStore", (t) => {
  const crateStore = readCrateStore(shortInput());
  t.deepEqual([["Z", "N"], ["M", "C", "D"], ["P"]], crateStore);
});

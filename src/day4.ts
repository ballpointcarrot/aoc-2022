import test from "ava";
import * as R from "remeda";
import { readInputLines } from "./util/input";
const shortInput = (): string[] => {
  return `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`.split("\n");
};

const createSet = (range: string): readonly number[] => {
  const [min, max] = range.split("-");
  const ary = new Array(parseInt(max) + 1 - parseInt(min));
  for (let i = 0; i < ary.length; i++) {
    ary[i] = parseInt(min) + i;
  }
  return ary;
};

const part1 = (input: string[]): number => {
  return input
    .map((pair) => {
      const [elf1, elf2] = pair.split(",").map(createSet);
      const intersection = R.intersection(elf1, elf2);
      if (R.equals(elf1, intersection) || R.equals(elf2, intersection)) {
        return 1;
      } else {
        return 0;
      }
    })
    .reduce((memo: number, a) => {
      return a + memo;
    }, 0);
};

const part2 = (input: string[]): number => {
  return input
    .map((pair) => {
      const [elf1, elf2] = pair.split(",").map(createSet);
      const intersection = R.intersection(elf1, elf2);
      if (intersection.length !== 0) {
        return 1;
      } else {
        return 0;
      }
    })
    .reduce((memo: number, a) => {
      return a + memo;
    }, 0);
};

test("part1 test data", (t) => {
  t.is(2, part1(shortInput()));
});

test("part1 full data", (t) => {
  const part1res = part1(readInputLines(4));
  console.log(`Day 4, Part 1: ${part1res}`);
  t.pass();
});

test("part2 test data", (t) => {
  t.is(4, part2(shortInput()));
});

test("part2 full data", (t) => {
  const part2res = part2(readInputLines(4));
  console.log(`Day 4, Part 2: ${part2res}`);
  t.pass();
});
test("createSet", (t) => {
  const testArr = [3, 4, 5, 6];
  t.deepEqual(testArr, createSet("3-6"));
});

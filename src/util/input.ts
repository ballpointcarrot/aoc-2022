import fs from "fs";

/**
 * Returns a string array (or string) containing
 * the given day's input.
 * @param day the day number
 * @return a string of the input file, or string array if split.
 */
export const readInput = (day: number, short: boolean = false): string => {
  return fs.readFileSync(`inputs/day${day}${short ? "-short" : ""}.txt`, "utf-8");
};

/**
 * Returns a string array containing the given day's input.
 * @param day the day number
 * @return string[] of the input split by line.
 */
export const readInputLines = (day: number, short: boolean = false): string[] => {
  const input = readInput(day, short);
  const lines = input.split("\n");
  if (lines[lines.length - 1] == "") {
    lines.pop();
  }
  return lines;
}

import test from "ava";

import * as R from "remeda";
import { readInputLines } from "./util/input";

const sampleInput = (): string[] => {
  return `$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k`.split("\n");
};

interface File {
    name: String;
    size?: number;
}

interface Dir extends File {
    files: Array<Dir|File>;
}

const parseInput = (input: string[]): Dir => {
    const rootDir: Dir = {
        name: '/',
        files: []
    };
    let currentDir = [rootDir];
    input.forEach(line => {
        const isDir = line.match(/^dir (.*)/);
        if(isDir) {
            R.last(currentDir)!.files.push({name: isDir[1],
                            files: []})
        }
        const fileData = line.match(/^(\d+)\s(.*)/);
        if(fileData) {
            R.last(currentDir)!.files.push({
                name: fileData[2],
                size: parseInt(fileData[1])
            })
        }
        const changeDir = line.match(/^\$\scd\s(.*)/);
        if(changeDir) {
            if(changeDir[1] === "/") {
                currentDir = [rootDir];
            } else {
                if(changeDir[1] === "..") {
                    currentDir.pop();
                } else {
                    const newDir = R.last(currentDir)!.files.find(f => f.name === changeDir[1]);
                    if(!newDir) {
                        throw new Error(`Missing directory ${newDir}!`);
                    }
                    currentDir.push(newDir as Dir);
                }
            }
        }
    })

    return rootDir;
}
const isDir = (f: unknown): f is Dir => {
    return Object.getOwnPropertyNames(f).includes("files");
}

type SizedDir = {d: Dir, totalSize: number};

const getDirSizes = (d: Dir): SizedDir[] => {
    let dirs: SizedDir[] = [];
    const dirSize = (d: Dir): number => {
        const totalSize = d.files.reduce((memo, f) => {
            if(isDir(f)) {
                return memo + dirSize(f);
            } else {
                return memo + (f.size ?? 0);
            }
        }, 0);
        dirs.push({d, totalSize});
        return totalSize;
    }
    dirSize(d);
    return dirs;
}

const part1 = (input: string[]): number => {
    const rootDir = parseInput(input);
    const MAX_SIZE = 100_000;
    const summableDirSizes = getDirSizes(rootDir).filter(d => d.totalSize < MAX_SIZE);

    return R.sumBy((d: SizedDir) => d.totalSize)(summableDirSizes);
};

const part2 = (input: string[]): number => {
    const TOTAL_AVAIL = 70_000_000;
    const UPDATE_ROOM = 30_000_000;
    const rootDir = parseInput(input);
    const dirSizes = getDirSizes(rootDir);
    const currentFree = TOTAL_AVAIL - dirSizes.find(d => d.d.name === "/")!.totalSize;
    const minDir = R.minBy((d:SizedDir) => {
        if(d.totalSize + currentFree < UPDATE_ROOM) {
            return TOTAL_AVAIL;
        } else {
            return (d.totalSize + currentFree) - UPDATE_ROOM;
        }
    })(dirSizes);
    return minDir!.totalSize;
};

test("part 1 sample input", (t) => {
  t.is(95437, part1(sampleInput()));
});

test("part 1 full input", (t) => {
  const part1res = part1(readInputLines(7));
  console.log(`Day 7, Part 1: ${part1res}`);
  t.pass();
});

test("part 2 sample input", (t) => {
  t.is(24933642, part2(sampleInput()));
});

test("part 2 full input", (t) => {
  const part2res = part2(readInputLines(7));
  console.log(`Day 7, Part 2: ${part2res}`);
  t.pass();
});

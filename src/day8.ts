import test from "ava";

import * as R from "remeda";
import { readInputLines } from "./util/input";

const sampleInput = (): string[] => {
  return `30373
25512
65332
33549
35390`.split("\n");
};

type Forest = number[][];

interface Coordinate {
  x: number;
  y: number;
}

const intoForest = (input: string[]): Forest => {
  return input.map((line) => {
    const nums = line.split("").map(Number);
    return nums;
  });
};

const isVisible = (f: Forest, coord: Coordinate): boolean => {
  let { x, y } = coord;
  const tree = f[y][x];
  let visible = true;
  // West
  while (x > 0) {
    x--;
    if (f[y][x] >= tree) {
      visible = false;
      break;
    }
  }
  if (visible) return visible;
  x = coord.x; // reset x
  // North
  visible = true;
  while (y > 0) {
    y--;
    if (f[y][x] >= tree) {
      visible = false;
      break;
    }
  }
  if (visible) return visible;
  y = coord.y; // reset x
  visible = true;
  // East
  while (x < f.length - 1) {
    x++;
    if (f[y][x] >= tree) {
      visible = false;
      break;
    }
  }
  if (visible) return visible;
  x = coord.x; // reset x
  // East
  visible = true;
  while (y < f[0].length - 1) {
    y++;
    if (f[y][x] >= tree) {
      visible = false;
      break;
    }
  }
  return visible;
};

const part1 = (input: string[]): number => {
  const forest: Forest = intoForest(input);
  const bools = R.map.indexed(forest, (ys, ypos) => {
    return R.map.indexed(ys, (xs, xpos) => {
      return isVisible(forest, { x: xpos, y: ypos });
    });
  });
  return bools.reduce((memo, ys) => {
    return (
      memo +
      ys.reduce((m2, x) => {
        return x ? m2 + 1 : m2;
      }, 0)
    );
  }, 0);
};

const scenicScores = (f: Forest): Forest => {
  return R.map.indexed(f, (ys, ypos) => {
    return R.map.indexed(ys, (tree, xpos) => {
      let westScore = 0;
      let cmpY = ypos;
      let cmpX = xpos;
      while (cmpY > 0) {
        cmpY--;
        westScore++;
        if (f[cmpY][xpos] >= tree) {
          break;
        }
      }
      cmpY = ypos; // Reset
      cmpX = xpos;
      let northScore = 0;
      while (cmpX > 0) {
        cmpX--;
        northScore++;
        if (f[ypos][cmpX] >= tree) {
          break;
        }
      }
      cmpY = ypos; // Reset
      cmpX = xpos;
      let eastScore = 0;
      while (cmpY < f.length - 1) {
        cmpY++;
        eastScore++;
        if (f[cmpY][xpos] >= tree) {
          break;
        }
      }
      cmpY = ypos; // Reset
      cmpX = xpos;
      let southScore = 0;
      while (cmpX < f[0].length - 1) {
        cmpX++;
        southScore++;
        if (f[ypos][cmpX] >= tree) {
          break;
        }
      }
      return westScore * northScore * eastScore * southScore;
    });
  });
};

const part2 = (input: string[]): number => {
  const forest = intoForest(input);
  const scores = scenicScores(forest);
  return scores.reduce((memo, ys) => {
    const highScore = Math.max(...ys);
    if (memo < highScore) {
      return highScore;
    } else {
      return memo;
    }
  }, 0);
};

test("part 1 sample input", (t) => {
  t.is(21, part1(sampleInput()));
});

test("part 1 full input", (t) => {
  const part1res = part1(readInputLines(8));
  console.log(`Day 8, Part 1: ${part1res}`);
  t.pass();
});

test("part 2 sample input", (t) => {
  t.is(8, part2(sampleInput()));
});

test("part 2 full input", (t) => {
  const part2res = part2(readInputLines(8));
  console.log(`Day 8, Part 2: ${part2res}`);
  t.pass();
});
